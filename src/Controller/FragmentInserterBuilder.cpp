#include "FragmentInserterBuilder.hpp"

FragInsertionStrategy::FragOptions
FragmentInserterBuilder::init_frag_files(Protinfo prot_info ) {
  FragInsertionStrategy::FragOptions frag_opt;
  core::pose::PoseOP pose_, best_pose_, native_pose_;
  core::pose::Pose ipose;
  core::fragment::FragSetOP frag_set_, frag_set_large;
  read_pose(prot_info.pdb_file, ipose);
  std::string ss = read_ss2(prot_info.ss_file, ipose);
  //std::cout << "start read fragment lib " << std::endl;
  frag_opt.ss = ss;
  read_frag_lib(prot_info.frag_3, frag_set_ );
  read_frag_lib(prot_info.frag_9, frag_set_large );
  frag_opt.frag_set_ = frag_set_;
  frag_opt.frag_set_large = frag_set_large;
  // core::pose::Pose my_pose;
  // core::import_pose::centroid_pose_from_pdb(my_pose, "./out1_align.pdb", false);
  // core::pose::PoseOP model_tmp = core::pose::PoseOP(new core::pose::Pose(my_pose));
  core::pose::PoseOP model_tmp = core::pose::PoseOP(new core::pose::Pose(ipose));
  frag_opt.native_model = model_tmp;
  read_density_map("map_1wit.mrc", ipose);
  return frag_opt;
}

boost::shared_ptr<FragInsertionMover>
FragmentInserterBuilder::get(std::string input_option, FragInsertionStrategy::FragOptions frag_opt) {
  boost::shared_ptr<FragInsertionMover> strategy_return;
  switch (fragment_insertion_strategy_map[input_option]) {
  case my_frag_insertion: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::my_frag_insertion, frag_opt);
    break;
  }
  case greedy_search: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::greedy_search, frag_opt);
    break;
  }
  case no_greedy_search: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::no_greedy_search, frag_opt);
    break;
  }
  case stage_rosetta_mover: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::stage_rosetta_mover, frag_opt);
    break;
  }
  case hybrid_mover: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::hybrid_mover, frag_opt);
    break;
  }
  case ILS_as_julia: {
    strategy_return = FragInsertionStrategy::get(FragInsertionStrategy::FragMoverTypes::ILS_as_julia, frag_opt);
    break;
  }
  default:
    std::cout << "no insertion strategy option" << std::endl;
    exit(1);
    break;
  }
  return strategy_return;
}
